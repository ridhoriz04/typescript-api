# User API Spec

# Register User

Endpoint : POST /api/users

Request Body

```json
{
  "username": "rizky",
  "password": "12345678",
  "name": "Ridho Rizky"
}
```

Response Body (Success)

```json
{
  "data": {
    "username": "rizky",
    "name": "Ridho Rizky"
  }
}
```

Response Body (Failed)

```json
{
  "error": "Username must not blank, ..."
}
```

# Login User

Endpoint : POST /api/users/login

Request Body

```json
{
  "username": "rizky",
  "password": "12345678"
}
```

Response Body (Success)

```json
{
  "data": {
    "username": "rizky",
    "name": "Ridho Rizky",
    "token": "uuid"
  }
}
```

Response Body (Failed)

```json
{
  "error": "Username and password is wrong!"
}
```

# Get User

Endpoint : GET /api/users/current

Request Header :

- X-API-TOKEN : token ()

Response Body (Success)

```json
{
  "data": {
    "username": "rizky",
    "name": "Ridho Rizky"
  }
}
```

Response Body (Failed)

```json
{
  "error": "Unauthorized..."
}
```

# Update User

Endpoint : PATCH /api/users/current

Request Header :

- X-API-TOKEN : token

Request Body :

```json
{
  "password": "12345678", // Tidak Wajib
  "name": "Ridho Rizky" // Tidak Wajib
}
```

Response Body (Success)

```json
{
  "data": {
    "username": "rizky",
    "name": "Ridho Rizky"
  }
}
```

Response Body (Failed)

```json
{
  "error": "Unauthorized, ..."
}
```

# Logout User

Endpoint : DELETE /api/users/current

Request Header :

- X-API-TOKEN : token

Response Body (Success)

```json
{
  "data": "OK"
}
```

Response Body (Failed)

```json
{
  "error": "Unauthorized, ..."
}
```
