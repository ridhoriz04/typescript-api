import { logger } from "./application/logging";
import { web } from "./application/web";

web.listen(5000, () => {
  logger.info("Server running on port 5000");
});
