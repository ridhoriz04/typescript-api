import express from "express";
import { UserController } from "../controller/user-controller";

export const publicRouter = express();

publicRouter.get("/", (req, res) => {
  res.status(200).json({
    message: "Hello World",
  });
});

publicRouter.post("/api/users", UserController.register);
publicRouter.post("/api/users/login", UserController.login);
