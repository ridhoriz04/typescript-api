import { web } from "./../src/application/web";
import supertest from "supertest";
import { logger } from "../src/application/logging";
import { ContactTest, UserTest } from "./test-util";

describe("POST /api/contacts", () => {
  beforeEach(async () => {
    await UserTest.create();
  });

  afterEach(async () => {
    await ContactTest.deleteAll();
    await UserTest.delete();
  });

  it("should be able to create contact", async () => {
    const response = await supertest(web)
      .post("/api/contacts")
      .set("X-API-TOKEN", "test")
      .send({
        first_name: "Ridho",
        last_name: "Rizky",
        email: "rizky@example.com",
        phone: "089999231",
      });

    logger.debug(response.body);
    expect(response.status).toBe(200);
    expect(response.body.data.id).toBeDefined();
    expect(response.body.data.first_name).toBe("Ridho");
    expect(response.body.data.last_name).toBe("Rizky");
    expect(response.body.data.email).toBe("rizky@example.com");
    expect(response.body.data.phone).toBe("089999231");
  });
});
