# Setup Project

Create .env file

Example :

```
DATABASE_URL="postgresql://root:password@localhost:5432/{yourdb}?schema=public"
```

Run Command Line

```shell
npm instal

npx prisma migrate dev

npx prisma generate

npm run build

npm run start
```
